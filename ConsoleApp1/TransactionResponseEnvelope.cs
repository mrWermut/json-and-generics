﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    internal class TransactionResponseEnvelope<TJson>
    {
        [JsonProperty("kktNumber")]
        public string kktNumber { get; set; }
        [JsonProperty("Error")]
        Error Error { get; set; }
        [JsonProperty("doc")]
        Doc<TJson> Doc { get; set; }
    }
}
