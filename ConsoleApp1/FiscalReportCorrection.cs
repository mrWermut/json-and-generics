﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class FiscalReportCorrection
    {
        public string fiscalDriveNumber { get; set; }
        public string kktRegId { get; set; }
        public string userInn { get; set; }
        public int fiscalDocumentNumber { get; set; }
        public DateTime dateTime { get; set; }
        public string fiscalSign { get; set; }
        public int encryptionSign { get; set; }
        public int offlineMode { get; set; }
        public int autoMode { get; set; }
        public int serviceSign { get; set; }
        public int bsoSign { get; set; }
        public int internetSign { get; set; }
        public int taxationType { get; set; }
        public int correctionReasonCode { get; set; }
        public string @operator { get; set; }
        public string cashierINN { get; set; }
        public string user { get; set; }
        public string machineNumber { get; set; }
        public int signPrinterInAutomat { get; set; }
        public int akzizSign { get; set; }
        public int gamblingSign { get; set; }
        public int lotterySign { get; set; }
        public string retailPlaceAddress { get; set; }
        public string placeMarket { get; set; }
        public string urlFNS { get; set; }
        public string senderAddress { get; set; }
        public string ofdName { get; set; }
        public string versionKKT { get; set; }
        public int versionFFDKKT { get; set; }
        public int versionFFD { get; set; }
        public string kktNumber { get; set; }
        public string ofdInn { get; set; }
        public string documentName { get; set; }
    }

}
