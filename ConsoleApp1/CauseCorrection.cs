﻿using System;

namespace  ConsoleApp1
{
    public class CauseCorrection
    {
        public string nameCauseCorrection { get; set; }
        public DateTime timeCauseCorrection { get; set; }
        public string docNumCauseCorr { get; set; }
    }
}