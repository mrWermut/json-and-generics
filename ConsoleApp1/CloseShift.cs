﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{

    public class CloseShift
    {
        public string FiscalDriveNumber { get; set; }
        public string KktRegId { get; set; }
        public string UserInn { get; set; }
        public int FiscalDocumentNumber { get; set; }
        public DateTime DateTime { get; set; }
        public string FiscalSign { get; set; }
        public int ShiftNumber { get; set; }
        public int ReceiptsQuantity { get; set; }
        public int DocumentsQuantity { get; set; }
        public int NotTransmittedDocumentsQuantity { get; set; }
        public DateTime NotTransmittedDocumentsDateTime { get; set; }
        public int FiscalDriveExhaustionSign { get; set; }
        public int FiscalDriveReplaceRequiredSign { get; set; }
        public int FiscalDriveMemoryExceededSign { get; set; }
        public int OfdResponseTimeoutSign { get; set; }
        public string @operator { get; set; }
        public string CashierINN { get; set; }
        public int VersionFFD { get; set; }
        public string DocumentName { get; set; }
        public string User { get; set; }
        public string RetailPlaceAddress { get; set; }
        public string PlaceMarket { get; set; }
    }
}
