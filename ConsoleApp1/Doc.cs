﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Doc<TJson>
    {
        [JsonProperty("fiscalDocumentNumber")]
        public int FiscalDocumentNumber { get; set; }
        [JsonProperty("fiscalSignShort")]
        public long FiscalSignShort { get; set; }
        [JsonProperty("DocType")]
        public string DocType { get; set; }
        [JsonProperty("SMS")]
        public string SMS { get; set; }
        [JsonProperty("JSON")]
        public TransactionResponseJsonEnvelope<TJson> Json { get; set; }
    }
}
