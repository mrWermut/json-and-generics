﻿using System;
using System.IO;
using System.Text;


namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {


            var path = "c:\\dev\\response1.json";
            var input = String.Empty;
            if (System.IO.File.Exists(path))
            { 
                using (var fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var streamReader = new StreamReader(fs, Encoding.UTF8))
                    {
                        input = streamReader.ReadToEnd();
                    }
                }

            }

            var result  =  Newtonsoft.Json.JsonConvert.DeserializeObject<TransactionResponseEnvelope<ReceiptCorrection>>(input);


             
            

            Console.ReadLine();



        }
    }
}
