﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
   
    public class FiscalReport
    {
     
        public string fiscalDriveNumber { get; set; }
        public string kktRegId { get; set; }
        public string userInn { get; set; }
        public int FiscalDocumentNumber { get; set; }
        public DateTime DateTime { get; set; }
        public string FiscalSign { get; set; }
        public int EncryptionSign { get; set; }
        public int OfflineMode { get; set; }
        public int AutoMode { get; set; }
        public int ServiceSign { get; set; }
        public int BsoSign { get; set; }
        public int InternetSign { get; set; }
        public int TaxationType { get; set; }
        public string @operator { get; set; }
        public string CashierINN { get; set; }
        public string User { get; set; }
        public string MachineNumber { get; set; }
        public int SignPrinterInAutomat { get; set; }
        public int akzizSign { get; set; }
        public int gamblingSign { get; set; }
        public int lotterySign { get; set; }
        public string retailPlaceAddress { get; set; }
        public string placeMarket { get; set; }
        public string urlFNS { get; set; }
        public string senderAddress { get; set; }
        public string ofdName { get; set; }
        public string versionKKT { get; set; }
        public int versionFFDKKT { get; set; }
        public int versionFFD { get; set; }
        public string kktNumber { get; set; }
        public string ofdInn { get; set; }
        public string documentName { get; set; }   
    }
}
