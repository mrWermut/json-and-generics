﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class CloseArchive
    {
        public string fiscalDriveNumber { get; set; }
        public string kktRegId { get; set; }
        public string userInn { get; set; }
        public int fiscalDocumentNumber { get; set; }
        public DateTime dateTime { get; set; }
        public string fiscalSign { get; set; }
        public string @operator { get; set; }
        public string cashierINN { get; set; }
        public int versionFFD { get; set; }
        public string user { get; set; }
        public string retailPlaceAddress { get; set; }
        public string placeMarket { get; set; }
        public int shiftNumber { get; set; }
        public string documentName { get; set; }
    }

}
