﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class ReceiptCorrection
    {
        public string fiscalDriveNumber { get; set; }
        public string kktRegId { get; set; }
        public string userInn { get; set; }
        public int fiscalDocumentNumber { get; set; }
        public DateTime dateTime { get; set; }
        public string fiscalSign { get; set; }
        public int shiftNumber { get; set; }
        public int requestNumber { get; set; }
        public int operationType { get; set; }
        public double totalSum { get; set; }
        public string @operator { get; set; }
        public string cashierINN { get; set; }
        public int typeCorrection { get; set; }
        public CauseCorrection causeCorrection { get; set; }
        public double nds18 { get; set; }
        public double cashTotalSum { get; set; }
    }
}
