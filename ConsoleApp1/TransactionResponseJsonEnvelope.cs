﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class TransactionResponseJsonEnvelope <TJson>
    {
        [JsonProperty("fiscalReport")]
        TJson FiscalReport { get; set; }
        [JsonProperty("fiscalReportCorrection")]
        TJson FiscalReportCorrection { get; set; }
        [JsonProperty("currentStateReport")]
        TJson CurrentStateReport { get; set; }

        [JsonProperty("closeArchive")]
        TJson CloseArchive { get; set; }

        [JsonProperty("openShift")]
        TJson OpenShift { get; set; }

        [JsonProperty("closeShift")]
        TJson CloseShift { get; set; }

        [JsonProperty("receiptCorrection")]
        TJson ReceiptCorrection { get; set; }




    }
}
